<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210418101619 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE colis (id INT AUTO_INCREMENT NOT NULL, fournisseur_id INT DEFAULT NULL, runsheet_id INT DEFAULT NULL, facture_id INT DEFAULT NULL, dispatch_id INT DEFAULT NULL, liste_retour_id INT DEFAULT NULL, console_id INT DEFAULT NULL, t_serv VARCHAR(255) NOT NULL, longeur INT NOT NULL, largeur INT NOT NULL, hauteur INT NOT NULL, poid DOUBLE PRECISION NOT NULL, t_colis VARCHAR(255) NOT NULL, t_enlev VARCHAR(255) NOT NULL, m_paie VARCHAR(255) NOT NULL, prix DOUBLE PRECISION NOT NULL, rq VARCHAR(255) NOT NULL, code_c VARCHAR(255) NOT NULL, etat VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, enlev_at DATETIME NOT NULL, hub_at DATETIME NOT NULL, deliv_at DATETIME NOT NULL, etats LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', destination VARCHAR(255) NOT NULL, anomalie LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', nom_c VARCHAR(255) DEFAULT NULL, prenom_c VARCHAR(255) DEFAULT NULL, tel_c VARCHAR(255) DEFAULT NULL, cpta INT DEFAULT NULL, INDEX IDX_470BDFF9670C757F (fournisseur_id), INDEX IDX_470BDFF9A73897F8 (runsheet_id), INDEX IDX_470BDFF97F2DEE08 (facture_id), INDEX IDX_470BDFF9C774D3B9 (dispatch_id), INDEX IDX_470BDFF975A2C546 (liste_retour_id), INDEX IDX_470BDFF972F9DD9F (console_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE console (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dispatch (id INT AUTO_INCREMENT NOT NULL, livreur_id INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8612665AF8646701 (livreur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE facture (id INT AUTO_INCREMENT NOT NULL, fournisseur_id INT DEFAULT NULL, createdat DATE DEFAULT NULL, INDEX IDX_FE866410670C757F (fournisseur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE liste_retour (id INT AUTO_INCREMENT NOT NULL, livreur_id INT DEFAULT NULL, created_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_DC1EA61AF8646701 (livreur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE retour (id INT AUTO_INCREMENT NOT NULL, idcolis INT NOT NULL, lib VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE runsheet (id INT AUTO_INCREMENT NOT NULL, livreur_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_46E7BEDDF8646701 (livreur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, tel VARCHAR(255) NOT NULL, state VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, zip INT DEFAULT NULL, fcon DATETIME DEFAULT NULL, username VARCHAR(255) NOT NULL, permis VARCHAR(255) DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, carte_g VARCHAR(255) DEFAULT NULL, cin VARCHAR(255) DEFAULT NULL, matricule VARCHAR(255) DEFAULT NULL, prix_liv DOUBLE PRECISION DEFAULT NULL, nom_comp VARCHAR(255) DEFAULT NULL, etat VARCHAR(255) DEFAULT NULL, enabled VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE colis ADD CONSTRAINT FK_470BDFF9670C757F FOREIGN KEY (fournisseur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE colis ADD CONSTRAINT FK_470BDFF9A73897F8 FOREIGN KEY (runsheet_id) REFERENCES runsheet (id)');
        $this->addSql('ALTER TABLE colis ADD CONSTRAINT FK_470BDFF97F2DEE08 FOREIGN KEY (facture_id) REFERENCES facture (id)');
        $this->addSql('ALTER TABLE colis ADD CONSTRAINT FK_470BDFF9C774D3B9 FOREIGN KEY (dispatch_id) REFERENCES dispatch (id)');
        $this->addSql('ALTER TABLE colis ADD CONSTRAINT FK_470BDFF975A2C546 FOREIGN KEY (liste_retour_id) REFERENCES liste_retour (id)');
        $this->addSql('ALTER TABLE colis ADD CONSTRAINT FK_470BDFF972F9DD9F FOREIGN KEY (console_id) REFERENCES console (id)');
        $this->addSql('ALTER TABLE dispatch ADD CONSTRAINT FK_8612665AF8646701 FOREIGN KEY (livreur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE866410670C757F FOREIGN KEY (fournisseur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE liste_retour ADD CONSTRAINT FK_DC1EA61AF8646701 FOREIGN KEY (livreur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE runsheet ADD CONSTRAINT FK_46E7BEDDF8646701 FOREIGN KEY (livreur_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE colis DROP FOREIGN KEY FK_470BDFF972F9DD9F');
        $this->addSql('ALTER TABLE colis DROP FOREIGN KEY FK_470BDFF9C774D3B9');
        $this->addSql('ALTER TABLE colis DROP FOREIGN KEY FK_470BDFF97F2DEE08');
        $this->addSql('ALTER TABLE colis DROP FOREIGN KEY FK_470BDFF975A2C546');
        $this->addSql('ALTER TABLE colis DROP FOREIGN KEY FK_470BDFF9A73897F8');
        $this->addSql('ALTER TABLE colis DROP FOREIGN KEY FK_470BDFF9670C757F');
        $this->addSql('ALTER TABLE dispatch DROP FOREIGN KEY FK_8612665AF8646701');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE866410670C757F');
        $this->addSql('ALTER TABLE liste_retour DROP FOREIGN KEY FK_DC1EA61AF8646701');
        $this->addSql('ALTER TABLE runsheet DROP FOREIGN KEY FK_46E7BEDDF8646701');
        $this->addSql('DROP TABLE colis');
        $this->addSql('DROP TABLE console');
        $this->addSql('DROP TABLE dispatch');
        $this->addSql('DROP TABLE facture');
        $this->addSql('DROP TABLE liste_retour');
        $this->addSql('DROP TABLE retour');
        $this->addSql('DROP TABLE runsheet');
        $this->addSql('DROP TABLE user');
    }
}
