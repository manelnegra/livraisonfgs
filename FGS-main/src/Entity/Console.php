<?php

namespace App\Entity;

use App\Repository\ConsoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConsoleRepository::class)
 */
class Console
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=Colis::class, mappedBy="console")
     */
    private $Colis;

    public function __construct()
    {
        $this->Colis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|Colis[]
     */
    public function getColis(): Collection
    {
        return $this->Colis;
    }

    public function addColi(Colis $coli): self
    {
        if (!$this->Colis->contains($coli)) {
            $this->Colis[] = $coli;
            $coli->setConsole($this);
        }

        return $this;
    }

    public function removeColi(Colis $coli): self
    {
        if ($this->Colis->removeElement($coli)) {
            // set the owning side to null (unless already changed)
            if ($coli->getConsole() === $this) {
                $coli->setConsole(null);
            }
        }

        return $this;
    }
}
