<?php

namespace App\Entity;

use App\Repository\ColisRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ColisRepository::class)
 */
class Colis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $t_serv;

    /**
     * @ORM\Column(type="integer")
     */
    private $longeur;

    /**
     * @ORM\Column(type="integer")
     */
    private $largeur;

    /**
     * @ORM\Column(type="integer")
     */
    private $hauteur;

    /**
     * @ORM\Column(type="float")
     */
    private $poid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $t_colis;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $t_enlev;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $m_paie;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rq;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="colis" ,cascade={"persist"})
     */
    private $fournisseur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code_c;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $enlev_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $hub_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $deliv_at;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $etats = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $destination;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $anomalie = [];

    /**
     * @ORM\ManyToOne(targetEntity=Runsheet::class, inversedBy="colis")
     */
    private $runsheet;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomC;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenomC;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telC;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cpta;

    /**
     * @ORM\ManyToOne(targetEntity=Facture::class, inversedBy="colis")
     */
    private $facture;

    /**
     * @ORM\ManyToOne(targetEntity=Dispatch::class, inversedBy="colis")
     */
    private $dispatch;

    /**
     * @ORM\ManyToOne(targetEntity=ListeRetour::class, inversedBy="colis")
     */
    private $listeRetour;

    /**
     * @ORM\ManyToOne(targetEntity=Console::class, inversedBy="Colis")
     */
    private $console;

    

    public function __construct()
    {
        $this->fournisseur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTServ(): ?string
    {
        return $this->t_serv;
    }

    public function setTServ(string $t_serv): self
    {
        $this->t_serv = $t_serv;

        return $this;
    }

    public function getLongeur(): ?int
    {
        return $this->longeur;
    }

    public function setLongeur(int $longeur): self
    {
        $this->longeur = $longeur;

        return $this;
    }

    public function getLargeur(): ?int
    {
        return $this->largeur;
    }

    public function setLargeur(int $largeur): self
    {
        $this->largeur = $largeur;

        return $this;
    }

    public function getHauteur(): ?int
    {
        return $this->hauteur;
    }

    public function setHauteur(int $hauteur): self
    {
        $this->hauteur = $hauteur;

        return $this;
    }

    public function getPoid(): ?float
    {
        return $this->poid;
    }

    public function setPoid(float $poid): self
    {
        $this->poid = $poid;

        return $this;
    }

    public function getTColis(): ?string
    {
        return $this->t_colis;
    }

    public function setTColis(string $t_colis): self
    {
        $this->t_colis = $t_colis;

        return $this;
    }

    public function getTEnlev(): ?string
    {
        return $this->t_enlev;
    }

    public function setTEnlev(string $t_enlev): self
    {
        $this->t_enlev = $t_enlev;

        return $this;
    }

    public function getMPaie(): ?string
    {
        return $this->m_paie;
    }

    public function setMPaie(string $m_paie): self
    {
        $this->m_paie = $m_paie;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getRq(): ?string
    {
        return $this->rq;
    }

    public function setRq(string $rq): self
    {
        $this->rq = $rq;

        return $this;
    }

    public function getFournisseur(): ?User
    {
        return $this->fournisseur;
    }

    public function setFournisseur(?User $fournisseur): self
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    public function getCodeC(): ?string
    {
        return $this->code_c;
    }

    public function setCodeC(string $code_c): self
    {
        $this->code_c = $code_c;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getEnlevAt(): ?\DateTimeInterface
    {
        return $this->enlev_at;
    }

    public function setEnlevAt(\DateTimeInterface $enlev_at): self
    {
        $this->enlev_at = $enlev_at;

        return $this;
    }

    public function getHubAt(): ?\DateTimeInterface
    {
        return $this->hub_at;
    }

    public function setHubAt(\DateTimeInterface $hub_at): self
    {
        $this->hub_at = $hub_at;

        return $this;
    }

    public function getDelivAt(): ?\DateTimeInterface
    {
        return $this->deliv_at;
    }

    public function setDelivAt(\DateTimeInterface $deliv_at): self
    {
        $this->deliv_at = $deliv_at;

        return $this;
    }

    public function getEtats(): ?array
    {
        return $this->etats;
    }

    public function setEtats(?array $etats): self
    {
        $this->etats = $etats;

        return $this;
    }

    public function getDestination(): ?string
    {
        return $this->destination;
    }

    public function setDestination(string $destination): self
    {
        $this->destination = $destination;

        return $this;
    }

    public function getAnomalie(): ?array
    {
        return $this->anomalie;
    }

    public function setAnomalie(?array $anomalie): self
    {
        $this->anomalie = $anomalie;

        return $this;
    }

    public function getRunsheet(): ?Runsheet
    {
        return $this->runsheet;
    }

    public function setRunsheet(?Runsheet $runsheet): self
    {
        $this->runsheet = $runsheet;

        return $this;
    }

    public function getNomC(): ?string
    {
        return $this->nomC;
    }

    public function setNomC(?string $nomC): self
    {
        $this->nomC = $nomC;

        return $this;
    }

    public function getPrenomC(): ?string
    {
        return $this->prenomC;
    }

    public function setPrenomC(?string $prenomC): self
    {
        $this->prenomC = $prenomC;

        return $this;
    }

    public function getTelC(): ?string
    {
        return $this->telC;
    }

    public function setTelC(?string $telC): self
    {
        $this->telC = $telC;

        return $this;
    }

    public function getCpta(): ?int
    {
        return $this->cpta;
    }

    public function setCpta(?int $cpta): self
    {
        $this->cpta = $cpta;

        return $this;
    }

    public function getFacture(): ?Facture
    {
        return $this->facture;
    }

    public function setFacture(?Facture $facture): self
    {
        $this->facture = $facture;

        return $this;
    }

    public function getDispatch(): ?Dispatch
    {
        return $this->dispatch;
    }

    public function setDispatch(?Dispatch $dispatch): self
    {
        $this->dispatch = $dispatch;

        return $this;
    }

    public function getListeRetour(): ?ListeRetour
    {
        return $this->listeRetour;
    }

    public function setListeRetour(?ListeRetour $listeRetour): self
    {
        $this->listeRetour = $listeRetour;

        return $this;
    }

    public function getConsole(): ?Console
    {
        return $this->console;
    }

    public function setConsole(?Console $console): self
    {
        $this->console = $console;

        return $this;
    }

}
