<?php

namespace App\Entity;

use App\Repository\RetourRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RetourRepository::class)
 */
class Retour
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idcolis;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lib;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdcolis(): ?int
    {
        return $this->idcolis;
    }

    public function setIdcolis(int $idcolis): self
    {
        $this->idcolis = $idcolis;

        return $this;
    }

    public function getLib(): ?string
    {
        return $this->lib;
    }

    public function setLib(string $lib): self
    {
        $this->lib = $lib;

        return $this;
    }
}
