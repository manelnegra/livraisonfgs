<?php

namespace App\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Snappy\Pdf;
use App\Entity\User;
use App\Entity\Colis;
use App\Entity\Dispatch;
use App\Entity\Runsheet;
use App\Repository\ColisRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RunsheetController extends AbstractController
{
    /**
     * @Route("/runsheet", name="new_runsheet")
     */
    public function index(Request $request): Response
    {
        $colise = $this->getDoctrine()->getRepository(Colis::class)->findBy(['etat' => "en stock"]);

        $colisr = $this->getDoctrine()->getRepository(Colis::class)->findBy(['etat' => "retour"]);
        dump($colise);

        dump($colisr);

        $colis1 = [];
        foreach ($colise as $colis) {
            $colis1[] = [
                'id' => $colis
            ];
        }


        foreach ($colisr as $colis) {
            $colis1[] = [
                'id' => $colis
            ];
        }
        dump($colis1);
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        $i = 0;
        $x = array();

        foreach ($users as $user) {
            if (in_array('ROLE_LIV', $user->getRoles())) {
                $x[$i] = $user;
                $i++;
            }
        }


        return $this->render('runsheet/new.html.twig', [
            'livreurs' => $x,
            'colis' => $colis1,
            'colisr' => $colisr,
            'x' => 1,
        ]);
    }

    /**
     * @Route("/runsheet_liv", name="runsheet_liv")
     */
    public function fiche(Request $request, SessionInterface $session, ColisRepository $colisRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $lcolis = $session->get('colis');

        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        $i = 0;
        $x = array();

        foreach ($users as $user) {
            if (in_array('ROLE_LIV', $user->getRoles())) {
                $x[$i] = $user;
                $i++;
            }
        }
        $currdate = new \DateTime();
        $currdate1 = $currdate->format("Y-m-d");
        $livs = $request->request->get('tliv');
        $code = $request->request->get('code_rc');


        if (!empty($runsheets)) {
            return $this->render("errors/runsheetError.html.twig");
        }

        $colis = $this->getDoctrine()
            ->getRepository(Colis::class)
            ->findOneBy(['id' => $code]);
        $lcolis[$code] = $colis;
        //            $colis->setRunsheet($runsheet);
        $liv = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['name' => $request->request->get('tliv')]);
        dump($liv);
        //$runsheet->setLivreur($liv) ;





        //$runsheet->setLivreur($liv) ; 
        // $em->persist($runsheet);dump($runsheet);$colis->setRunsheet($runsheet);
        // $em->flush();



        $session->set('colis', $lcolis);

        $colis1 = [];
        foreach ($lcolis as $code => $colis) {
            $colis1[] = [
                'id' => $colisRepository->find($colis->getId())
            ];
        }




        return $this->render("runsheet/new1.html.twig", [
            'colis' => $colis1,
            'livreurs' => $x,
            'x' => 1,
            'liv' => $liv->getId()
        ]);
    }

    /**
     * @Route("/create_run", name="create_run")
     */
    public function create(Request $request, ColisRepository $colisRepository , \Knp\Snappy\Pdf $snappy)
    {
        $em = $this->getDoctrine()->getManager();
        $runsheet = new Runsheet();
        $runsheet->setCreatedAt(new \DateTime("now"))
            ->setCode("jjj");
        $idliv = $request->request->get('tliv');
        $liv = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['name' => $idliv]);
        $disp = $this->getDoctrine()->getRepository(Dispatch::class)->findOneBy(['livreur' => $liv]);
        
        $runsheet->setLivreur($liv);
        $colisp = $this->getDoctrine()->getRepository(Colis::class)->findBy(['etat' => "dispatched", "dispatch" => $disp]);
        
        $em->persist($runsheet);

        
        $idcolis = $request->request->get('code_rc');
        $arr = explode("/", $idcolis);
        array_pop($arr);
        dump($runsheet);
        $colis1 = [];
        for ($i = 0; $i < count($arr); $i++) {
            $colis4 = $colisRepository->find($arr[$i]);
            $colis4->setRunsheet($runsheet);
            if ($colis4->getEtat() == "en stock")
                $colis4->setEtat("en cours de livraison");
            if ($colis4->getEtat() == "retour")
                $colis4->setEtat("en cours de retour");
            if ($colis4->getEtat() == "dispatched")
                $colis4->setEtat("en cours de enlevement");

            $colis1[] = [
                'id' => $colis4
            ];
        }
        foreach ($colisp as $colis) {
            $colis->setRunsheet($runsheet);
        }
        foreach ($colisp as $colis) {
            $colis1[] = [
                'id' => $colis
            ];
        }
        $em->flush();

        $barcode = new BarcodeGenerator();
        $barcode->setText("A" . $runsheet->getId() . "A");
        $barcode->setType(BarcodeGenerator::Codabar);
        $barcode->setScale(2);
        $barcode->setThickness(25);
        $barcode->setFontSize(10);
        $barcode->setFilename("codeabars/runsheets/" . $runsheet->getId() . ".png");
        $barcode->setFormat('PNG');
        $code = $barcode->generate();
        // Configure Dompdf according to your needs
       /* $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->set('isRemoteEnabled',TRUE);  

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);
        $dompdf->setPaper('A4' , 'portrait');
        
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('runsheet/model.html.twig', [
            'title' => "Welcome to our PDF Test",
            'livreur' => $liv,
            'colis' => $colis1,
            'filename' => 'codeabars/runsheets/' . $runsheet->getId() . '   .png',
            'x' => 1
        ]);

        $html.='    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">';

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);*/

            dump($colis1);
        
        $html = $this->render("runsheet/model.html.twig", [
            'title' => "Welcome to our PDF Test",
            'livreur' => $liv,
            'colis' => $colis1,
            'runsheetid' => $runsheet->getId(),
            'filename' => 'codeabars/runsheets/' . $runsheet->getId() . '   .png',
            'x' => 1
        ]);
        $filename = 'SnappyPDF';
        return new Response(
            $snappy->getOutputFromHtml($html),200,array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"',
                'image' => true
            )
        );
 
    }
}
