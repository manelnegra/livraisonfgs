<?php

namespace App\Controller;

use App\Entity\Colis;
use App\Entity\Console;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ConsoleController extends AbstractController
{
    /**
     * @Route("/console", name="console")
     */
    public function index(): Response
    {

        $console = new Console();
        $colis = $this->getDoctrine()->getRepository(Colis::class)->findBy(['etat' => "en attente e"]);
        return $this->render('console/index.html.twig', [
            'colis' => $colis,
            'x' => 1,
            'i' => 0
        ]);
    }

    /**
     * @Route("/create_console/", name="create_console")
     */

    public function createConsole(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ids = $request->request->get('inp');
        $arr = explode("/", $ids);
        array_pop($arr);
        $console = new Console;
        $console->setCreatedAt(new \DateTime());
        $a=[];
        for ($i = 0; $i < count($arr); $i++) {
            $colis = $this->getDoctrine()->getRepository(Colis::class)->findOneBy(['id' => (int)$arr[$i]]);
            $a = [
                $i => $colis
            ];
            $console->addColi($colis);
        }
        $em->persist($console);
        $em->flush();
        $barcode = new BarcodeGenerator();
        $barcode->setText("A".$console->getId()."A");
        $barcode->setType(BarcodeGenerator::Codabar);
        $barcode->setScale(2);
        $barcode->setThickness(25);
        $barcode->setFontSize(10);
        $barcode->setFilename("codeabars/console/".$console->getId().".png");
        $barcode->setFormat('PNG');
        $code = $barcode->generate();
        return $this->render("console/liste.html.twig", [
            'colis' => NULL,
            'x' => 0
        ]);
    }

    /**
     * @Route("liste_console" , name="liste_console")
     */

    public function listeConsole(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('tcons');
        dump($id);
        $colis= new Colis;
        if($id != NULL){
            $colis = $this->getDoctrine()->getRepository(Colis::class)->findBy([
                'console' => $id
            ]);

            foreach ($colis as $coli) {
                $coli->setEtat("en stock");
                $em->persist($coli);
            }
        }
            dump($colis);
            
        $em->flush();

        return $this->render("console/liste1.html.twig", [
            'colis' => $colis,
            'x' => 0
        ]);
    }
}
