<?php

namespace App\Controller;

use App\Entity\User;


use App\Entity\Colis;
use App\Entity\Dispatch;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DispatchController extends AbstractController
{
    /**
     * @Route("/dispatch", name="dispatch")
     */
    public function index(Request $request): Response
    {

        $colis = $this->getDoctrine()->getRepository(Colis::class)->findBy(['etat' => "en attente e"]);
        dump($colis);
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
            dump($users);
        $i = 0;
        $x = array();

        foreach ($users as $user) {
            if (in_array('ROLE_LIV', $user->getRoles())) {
                $x[$i] = $user;
                $i++;
            }
        }

        $i=0;
        $j=count($colis);
        
        return $this->render('dispatch/index.html.twig', [
            'livreurs' => $x,
            'colis' => $colis,
            'x' => 1,
            'i' => $i,
            'j' => $j,
            
        ]);
    }

    /**
     * @Route("/dispatch/confirm" , name="confirm")
     */

     public function confirm(Request $request){
         $em = $this->getDoctrine()->getManager();
        $ids = $request->request->get('inp');
        $arr=explode("/" , $ids);
        array_pop($arr);
        $liv = $request->request->get('tliv');
        dump($liv);
        $liv1 = $this->getDoctrine()->getRepository(User::class)->findOneBy(['name'=>$liv]);
        dump($liv1);
        
        $dispatch = new Dispatch();
        $dispatch->setCreatedAt(new \DateTime());
        for($i=0 ; $i< count($arr) ; $i++){
            $dispatch->setLivreur($liv1);
            
            $colis = $this->getDoctrine()->getRepository(Colis::class)->findOneBy(['id' => (int)$arr[$i]]);
            $dispatch->addColi($colis);
            $em->persist($dispatch);
            $colis->setEtat("dispatched");

        }
        $em->flush();
        return $this->redirectToRoute("home");
     }
}
