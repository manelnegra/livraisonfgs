<?php

namespace App\Controller;

use App\Entity\Colis;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(){
        
        $user = $this->getUser();

        $colis=$this->getDoctrine()
        ->getRepository(Colis::class)
        ->findAll();

        $colis_c=$this->getDoctrine()
        ->getRepository(Colis::class)
        ->findBy(['etat' => 'en attente e']);

        
        $colis_e=$this->getDoctrine()
        ->getRepository(Colis::class)
        ->findBy(['etat' => 'enleve']);
        
        
        $colis_a=$this->getDoctrine()
                ->getRepository(Colis::class)
                ->findColisAnomalie();
        
        $colis_h=$this->getDoctrine()
                ->getRepository(Colis::class)
                ->findBy(['etat' => 'en hub']);

        
        $colis_l=$this->getDoctrine()
                ->getRepository(Colis::class)
                ->findBy(['etat' => 'livre']);        

        return $this->render('index/index.html.twig',[
            'colis_c' => $colis_c,
            'colis' =>$colis,
            'colis_e' => $colis_e,
            'colis_h' => $colis_h,
            'colis_l' => $colis_l,
            'colis_a' => $colis_a,
            'user' => $user
            
        ]);
    }
   
}
