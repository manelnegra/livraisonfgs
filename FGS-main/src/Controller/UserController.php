<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController extends AbstractController
{
    function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
    
        $all = '';
        $password = '';
        foreach($sets as $set)
        {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
    
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
    
        $password = str_shuffle($password);
    
        if(!$add_dashes)
            return $password;
    
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($password) > $dash_len)
        {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }
    
    function generate_unique_username($a, $b, $c){
        $firstname      = $a;//data coming from user
        $lastname       = $b;//data coming from user
        $id       = $c;//data coming from user
        $new_username   = $firstname.$lastname.$id;
    
        /* Note: writing here pseudo sql code, replace with the actual php mysql query syntax */
    
        return $new_username;
    }
    /**
     * @Route("/liste_user", name="liste_user")
     */
    public function index(): Response
    {
        
        $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->findAll();
                dump($user);
        return $this->render('user/liste.html.twig', [
            'users' => $user,
            'x' => 1
        ]);
    }

    /**
     * @Route("/creer_livreur", name="creer_livreur")
     */
    public function creer_livreur(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer){

        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        $pass=$this->generateStrongPassword();
            $username=$this->generate_unique_username($request->request->get('prenom'),$request->request->get('nom'),$user->getId());
       /* if ($request->request->count() > 0) {
            // encode the plain <passwordx></passwordx>
            $pass=$this->generateStrongPassword();
            $username=$this->generate_unique_username($request->request->get('prenom'),$request->request->get('nom'),$user->getId());
            $user->setName($request->request->get('prenom'))
                 ->setLastname($request->request->get('nom'))
                 ->setTel($request->request->get('tel'))    
                 ->setState($request->request->get('gouv'))
                 ->setCity($request->request->get('ville'))
                 ->setZip((int)$request->request->get('zip'))
                 ->setUsername($username)
                 ->setPassword(
                    $passwordEncoder->encodePassword(
                         $user,
                         $pass
                     )
                 )
                 ->setEmail($request->request->get('mail'));
                 $entityManager = $this->getDoctrine()->getManager();
                 $entityManager->persist($user);
                 $entityManager->flush();
                 $username=$this->generate_unique_username($request->request->get('prenom'),$request->request->get('nom'),$user->getId());
                 $user->setUsername($username);
                 $entityManager->persist($user);
                 $entityManager->flush();*/
                        $slugger = new AsciiSlugger();
                 if ($form->isSubmitted() && $form->isValid()) {
                    /** @var UploadedFile $permisFile */
                    $permisFile = $form->get('permis')->getData();
        
                    // this condition is needed because the 'brochure' field is not required
                    // so the PDF file must be processed only when a file is uploaded
                    if ($permisFile) {
                        $originalFilename = pathinfo($permisFile->getClientOriginalName(), PATHINFO_FILENAME);
                        // this is needed to safely include the file name as part of the URL
                        $safeFilename = $slugger->slug($originalFilename);
                        $newFilename = $safeFilename.'-'.uniqid().'.'.$permisFile->guessExtension();
        
                        // Move the file to the directory where brochures are stored
                        try {
                            $permisFile->move(
                                $this->getParameter('permis_directory'),
                                $newFilename
                            );
                        } catch (FileException $e) {
                            // ... handle exception if something happens during file upload
                        }
        
                        // updates the 'brochureFilename' property to store the PDF file name
                        // instead of its contents
                        $user->setPermis($newFilename);
                    }
                        dump($request);
                    $user->setName($form->get('name')->getData())
                 ->setLastname($form->get('lastname')->getData())
                 ->setTel($form->get('tel')->getData())    
                 ->setUsername($username)
                 ->setPassword(
                    $passwordEncoder->encodePassword(
                         $user,
                         $pass
                     )
                 )
                 ->setEmail($form->get('email')->getData())
                 ->setRoles(["ROLE_LIV"]);
                 $entityManager = $this->getDoctrine()->getManager();
                 $entityManager->persist($user);
                 $entityManager->flush();
                 $username=$this->generate_unique_username($request->request->get('prenom'),$request->request->get('nom'),$user->getId());
                 $user->setUsername($username);
                 $entityManager->persist($user);
                 $entityManager->flush();
        
                    
                }

                $message = (new \Swift_Message('Hello Email'))
                ->setFrom('marvelamir@gmail.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        // templates/hello/email.txt.twig
                        'email/mail.txt.twig',
                        ['name' => "amir",
                           'username' => $username,
                           'pass' => $pass
                        ]
                    )
                )
            ;
           
            $mailer->send($message);
                 
                 


                 
                
                return $this->render('user/formulaire_livreur.html.twig',[
                    'form' => $form->createView()
                ]);
}

/**
 * @Route("/user_enable/{id}" , name="user_enable")
 */
    public function userEnable($id){
        $em=$this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);

        if($user->getEtat() == "Enabled"){
            $user->setEtat("Disabled");
            $user->setEnabled("no");
        }
        
        else if($user->getEtat() == "Disabled"){
            $user->setEtat("Enabled");
            $user->setEnabled("yes");
        }

        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute("liste_user");
    }
}
