<?php

namespace App\Controller;

use DateTime;
use App\Entity\User;
use App\Entity\Colis;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RecetteController extends AbstractController
{
    /**
     * @Route("/recette", name="recette")
     */
    public function index(): Response
    {
        $colis = $this->getDoctrine()->getRepository(Colis::class)->findBy(['etat' => 'Livre']);
        $s = 0;
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        $i = 0;
        $x = array();

        foreach ($users as $user) {
            if (!in_array('ROLE_LIV', $user->getRoles()) && !in_array('ROLE_ADMIN', $user->getRoles())) {
                $x[$i] = $user;
                $i++;
            }
        }
        foreach ($colis as $col) {
            $s = $s + $col->getPrix() ;
        }
        dump($s);
        $dat = new \DateTime();
        return $this->render('recette/index.html.twig', [
            's' => $s ,
            'colis' => $colis,
            'x' => 1,
            'dat' => $dat,
            'livreurs' => $x
        ]);
    }

    /**
     * @Route("/recette_fourn" , name="recette_fourn")
     */
    public function recettef(Request $request)
    {
        $fourn=$request->request->get('tliv');
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['name' => $fourn]);
            $users1 = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
            
        $i = 0;
        $x = array();
        foreach ($users1 as $user) {
            if (!in_array('ROLE_LIV', $user->getRoles()) && !in_array('ROLE_ADMIN', $user->getRoles()) ) {
                $x[$i] = $user;
                $i++;
            }
        }
        $colis=$this->getDoctrine()->getRepository(Colis::class)->findBy(['fournisseur' => $users->getId(), 'etat' => "Livre"]);
        $s=0;
        foreach ($colis as $col) {
        
            $s = $s + $col->getPrix() ;
            $x = $col->getFournisseur()->getPrixLiv() ;
        }
       
        $s=84.3;
        return $this->render("recette/listeF.html.twig", [
            'livreurs' => $x,
            'colis' => $colis,
            'x' => 1,
            'dat' => new DateTime,
            's' => $s
            

        ]);
    }

    /**
     * @Route("/recette_fourn1" , name="recette_fourn1")
     */

    public function list(Request $request)
    {
        $dat = $request->request->get('date');
        $fourn = $request->request->get('tfourn');
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        $i = 0;
        $x = array();
        foreach ($users as $user) {
           
            if (!in_array('ROLE_LIV', $user->getRoles()) && !in_array('ROLE_ADMIN', $user->getRoles())) {
                $x[$i] = $user;
                $i++;
            }
        }


        $fournis = $this->getDoctrine()->getRepository(User::class)->findOneBy(['name' => $fourn]);

        $colis = $this->getDoctrine()->getRepository(Colis::class)->findBy(['fournisseur' => $fournis]);
        dump($fournis);
        return $this->render("recette/new1.html.twig", [
            'livreurs' => $users,
            'colis' =>$colis,
            'x' => 1,

        ]);
    }
}
