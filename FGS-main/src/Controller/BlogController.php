<?php

namespace App\Controller;

use App\Entity\Colis;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;

class BlogController extends AbstractController
{


    

    /**
     * @Route("/colis_en_attente", name="colis_en_attente")
     */
    public function colis_en_attente(\Knp\Snappy\Pdf $snappy){
        $html = $this->render("colis/model.html.twig", [
            'title' => "Welcome to our PDF Test",

        ]);
        $filename = 'SnappyPDF';
        return new Response(
            $snappy->getOutputFromHtml($html),200,array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"',
                'image' => true
            )
        );
    }


      /**
     * @Route("/colis_enlevement", name="colis_enlevement")
     */
    public function colis_enlevement(){
        return $this->render('blog/colis_enlevement.html.twig');
    }

      /**
     * @Route("/colis_livrée", name="colis_livrée")
     */
    public function colis_livrée(){
        return $this->render('blog/colis_livrée.html.twig');
    }

      /**
     * @Route("/colis_annule", name="colis_annule")
     */
    public function colis_annule(){
        return $this->render('blog/colis_annule.html.twig');
    }

    /**
     * @Route("/index3", name="index3")
     */
    public function index3(){
   
        return $this->render('blog/index3.html.twig');
    }


    
    
   
    
}
