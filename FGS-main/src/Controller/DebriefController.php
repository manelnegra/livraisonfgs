<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Colis;
use App\Entity\Retour;
use App\Entity\Runsheet;
use App\Repository\ColisRepository;
use App\Repository\RunsheetRepository;
use App\Repository\UserRepository;
use Doctrine\Migrations\Query\Query;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DebriefController extends AbstractController
{
    /**
     * @Route("/debrief", name="debrief")
     */
    public function index(Request $request): Response
    {
        $colis = $this->getDoctrine()->getRepository(Colis::class)->findAll();

        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        $i = 0;
        $x = array();

        foreach ($users as $user) {
            if (in_array('ROLE_LIV', $user->getRoles())) {
                $x[$i] = $user;
                $i++;
            }
        }
        dump($x);

        return $this->render('debrief/index.html.twig', [
            'livreurs' => $x,
            'colis' => $colis,
            'x' => 1,
        ]);
    }

    /**
     * @Route("/debrief_liv", name="debrief_liv")
     */
    public function fiche(Request $request, SessionInterface $session, ColisRepository $colisRepository, UserRepository $UserRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $lcolis = $session->get('colis');
        $date = $request->request->get('date');
        dump($date);
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        $i = 0;
        $x = array();

        foreach ($users as $user) {
            if (in_array('ROLE_LIV', $user->getRoles())) {
                $x[$i] = $user;
                $i++;
            }
        }

        $code = $request->request->get('code_rc');
        $livid = $request->request->get('tliv');
        $livs = $UserRepository->findOneBy(['name' => $livid]);
        dump($livid);
        $runsheets = $this->getDoctrine()
            ->getRepository(Runsheet::class)
            ->findOneBy(['livreur' => $livs->getId()]);
        dump($runsheets);

        if (empty($runsheets))
            return $this->render("errors/runsheetError.html.twig");
        $colisr = $this->getDoctrine()
            ->getRepository(Colis::class)
            ->findBy(['runsheet' => $runsheets->getId()]);

        $j = 0;
        foreach ($colisr as $coli)
            $j++;


        $colis = $this->getDoctrine()
            ->getRepository(Colis::class)
            ->findOneBy(['id' => $code]);
        $lcolis[$code] = $colis;
        //            $colis->setRunsheet($runsheet);
        $liv = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['name' => $request->request->get('tliv')]);
        dump($liv);
        //$runsheet->setLivreur($liv) ;





        //$runsheet->setLivreur($liv) ; 
        // $em->persist($runsheet);dump($runsheet);$colis->setRunsheet($runsheet);
        // $em->flush();



        $session->set('colis', $lcolis);
        dump($lcolis);
        $colis1 = [];

        foreach ($lcolis as $code => $colis) {
            $colis1[] = [
                'id' => $colisRepository->find($colis->getId())
            ];
        }



        dump($j);

        return $this->render("debrief/index1.html.twig", [
            'colis' => $colis1,
            'livreurs' => $x,
            'x' => 1,
            'liv' => $liv->getId(),
            'date' => $date,
            'cmpt' => $j
        ]);
    }

    /**
     * @Route("/create_debrief", name="create_debrief")
     */
    public function create(Request $request, ColisRepository $colisRepository, RunsheetRepository $RunsheetRepository)
    {
        $em = $this->getDoctrine()->getManager();


        $colise = $request->request->get('liv');
        $colisp = $request->request->get('enl');
        $liv=$request->request->get('tliv');
        $str="";

        $arr = explode("/", $colise);
        array_pop($arr);

        $arr1 = explode("/", $colisp);
        array_pop($arr1);


        $etat = $request->request->get('etat');
        $a = $request->request->get('anom');
        $e = [];
        $t = [];

        for ($i = 0; $i < count($arr); $i++) {
            array_push($e, $colisRepository->find($arr[$i]));
        }
        for ($i = 0; $i < count($arr1); $i++) {
            array_push($t, $colisRepository->find($arr1[$i]));
        }
        $idr = NULL;
        if ($etat == "Livre") {
            foreach ($e as $colis3) {
                $colis4 = $colisRepository->find($colis3->getId());

                $colis4->setEtat($etat);
                $str=$str.$colis4->getId()."-";
                $qb = $em->createQuery('select r from App\Entity\Runsheet r where r.id = ' . $colis4->getRunsheet()->getId());
                $runsheets = $qb->getResult();
                $idr = $runsheets[0]->getId();
                foreach ($runsheets as $runsheet) {
                    $colis4->setRunsheet(NULL);
                }
            }
           
        } else if ($etat == "enleve") {
            foreach ($t as $colis3) {
                $colis4 = $colisRepository->find($colis3->getId());
                $colis4->setEtat("en stock");
                $qb = $em->createQuery('select r from App\Entity\Runsheet r where r.id = ' . $colis4->getRunsheet()->getId());
                $runsheets = $qb->getResult();
                $idr = $runsheets[0]->getId();
                foreach ($runsheets as $runsheet) {
                    $colis4->setRunsheet(NULL);
                }
            }
        } else {
        if(empty($e)){
            foreach ($t as $colis3) {
                $colis4 = $colisRepository->find($colis3->getId());
                
                if ($a == "Colis refusé par le client" || $a == "Colis refusé par le fournisseur" || $a == "Colis refusé") {

                    $colis4->setCpta(3);
                    $r = new Retour();
                    $r->setIdcolis($colis4->getId());
                    $r->setLib($a);
                    $em->persist($r);
                    $em->flush();

                    $colis4->setEtat($etat);
                    $qb = $em->createQuery('select r from App\Entity\Runsheet r where r.id = ' . $colis4->getRunsheet()->getId());
                    $runsheets = $qb->getResult();
                    $idr = $runsheets[0]->getId();
                    foreach ($runsheets as $runsheet) {
                        $colis4->setRunsheet(NULL);
                    }
                    $this->redirectToRoute("retour");
                }



                if ($colis4->getCpta() < 3 || $colis4->getCpta() == NULL) {
                    $colis4->setCpta($colis4->getCpta() + 1);
                    $colis4->setEtat("en stock");

                    if ($colis4->getCpta() == 3) {


                        $qb = $em->createQuery('select r from App\Entity\Runsheet r where r.id = ' . $colis4->getRunsheet()->getId());
                        $runsheets = $qb->getResult();
                        $idr = $runsheets[0]->getId();
                        foreach ($runsheets as $runsheet) {
                            $colis4->setRunsheet(NULL);
                        }
                        $r = new Retour();
                        $r->setIdcolis($colis4->getId());
                        $r->setLib($a);
                        $colis4->setEtat("Planification retour");
                        $em->persist($r);
                        $this->redirectToRoute("retour");
                    } else {

                        $qb = $em->createQuery('select r from App\Entity\Runsheet r where r.id = ' . $colis4->getRunsheet()->getId());
                        $runsheets = $qb->getResult();
                        $idr = $runsheets[0]->getId();
                        foreach ($runsheets as $runsheet) {
                            $colis4->setRunsheet(NULL);
                        }
                    }
                }
            }
        }else if (empty($t)){
            foreach ($e as $colis3) {
                $colis4 = $colisRepository->find($colis3->getId());
                
                if ($a == "Colis refusé par le client" || $a == "Colis refusé par le fournisseur" || $a == "Colis refusé") {

                    $colis4->setCpta(3);
                    $r = new Retour();
                    $r->setIdcolis($colis4->getId());
                    $r->setLib($a);
                    $em->persist($r);
                    $em->flush();

                    $colis4->setEtat($etat);
                    $qb = $em->createQuery('select r from App\Entity\Runsheet r where r.id = ' . $colis4->getRunsheet()->getId());
                    $runsheets = $qb->getResult();
                    $idr = $runsheets[0]->getId();
                    foreach ($runsheets as $runsheet) {
                        $colis4->setRunsheet(NULL);
                    }
                    $this->redirectToRoute("retour");
                }



                if ($colis4->getCpta() < 3 || $colis4->getCpta() == NULL) {
                    $colis4->setCpta($colis4->getCpta() + 1);
                    $colis4->setEtat("en stock");

                    if ($colis4->getCpta() == 3) {


                        $qb = $em->createQuery('select r from App\Entity\Runsheet r where r.id = ' . $colis4->getRunsheet()->getId());
                        $runsheets = $qb->getResult();
                        $idr = $runsheets[0]->getId();
                        foreach ($runsheets as $runsheet) {
                            $colis4->setRunsheet(NULL);
                        }
                        $r = new Retour();
                        $r->setIdcolis($colis4->getId());
                        $r->setLib($a);
                        $colis4->setEtat("Planification retour");
                        $em->persist($r);
                        $this->redirectToRoute("retour");
                    } else {

                        $qb = $em->createQuery('select r from App\Entity\Runsheet r where r.id = ' . $colis4->getRunsheet()->getId());
                        $runsheets = $qb->getResult();
                        $idr = $runsheets[0]->getId();
                        foreach ($runsheets as $runsheet) {
                            $colis4->setRunsheet(NULL);
                        }
                    }
                }
            }
        }
            
        }
        $em->flush();

        $runsh = new Runsheet;
        $runsh = $runsheets[0];

        if (empty($this->getDoctrine()->getRepository(Colis::class)->findBy(['runsheet' => $RunsheetRepository->find($idr)]))){
            $em->remove($runsheets[0]);
            $em->flush();
            if($str == "")
                $str="*";
            return $this->redirectToRoute('recette_liv',[
                'liv' => $liv,
                'colis' => $str
            ]);
            
        }



        $em->flush();


        return $this->redirectToRoute('debrief');
    }

    /**
     * @Route("/recette_liv/{liv}/{colis}" , name="recette_liv")
     */

     public function recetteliv($liv,$colis, ColisRepository $colisRepository){
        $arr = explode("-", $colis);
        array_pop($arr);
        $e=[];
        $s = 0;
        
        if(!empty($arr)){

            for ($i = 0; $i < count($arr); $i++) {
                array_push($e, $colisRepository->find($arr[$i]));
                
                $s+=$e[$i]->getPrix();
            }
        }
         return $this->render("recette/recetteliv.html.twig",[
             'colis' => $e,
             's' => $s,
             'x' => 1
         ]);
     }
}
