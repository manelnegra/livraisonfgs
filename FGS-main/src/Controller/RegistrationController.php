<?php

namespace App\Controller;

use App\Entity\User;
use DateTimeInterface;
use App\Form\RegistrationFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
{
	$sets = array();
	if(strpos($available_sets, 'l') !== false)
		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
	if(strpos($available_sets, 'u') !== false)
		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
	if(strpos($available_sets, 'd') !== false)
		$sets[] = '23456789';
	if(strpos($available_sets, 's') !== false)
		$sets[] = '!@#$%&*?';

	$all = '';
	$password = '';
	foreach($sets as $set)
	{
		$password .= $set[array_rand(str_split($set))];
		$all .= $set;
	}

	$all = str_split($all);
	for($i = 0; $i < $length - count($sets); $i++)
		$password .= $all[array_rand($all)];

	$password = str_shuffle($password);

	if(!$add_dashes)
		return $password;

	$dash_len = floor(sqrt($length));
	$dash_str = '';
	while(strlen($password) > $dash_len)
	{
		$dash_str .= substr($password, 0, $dash_len) . '-';
		$password = substr($password, $dash_len);
	}
	$dash_str .= $password;
	return $dash_str;
}

function generate_unique_username($a, $b, $c){
    $firstname      = $a;//data coming from user
    $lastname       = $b;//data coming from user
    $id       = $c;//data coming from user
    $new_username   = $firstname.$lastname.$id;

    /* Note: writing here pseudo sql code, replace with the actual php mysql query syntax */

    return $new_username;
}
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder , \Swift_Mailer $mailer): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        
        if ($request->request->count() > 0) {
            // encode the plain <passwordx></passwordx>
            $fcon=new \DateTime(date("d/m/Y" , strtotime($request->request->get('fcon'))));
            $pass=$this->generateStrongPassword();
            $username=$this->generate_unique_username($request->request->get('prenom'),$request->request->get('nom'),$user->getId());
            $user->setName($request->request->get('prenom'))
                 ->setLastname($request->request->get('nom'))
                 ->setNomComp($request->request->get('nom_comp'))
                 ->setPrixLiv($request->request->get('prix_liv'))
                 ->setFcon($fcon)
                 ->setTel($request->request->get('tel'))    
                 ->setState($request->request->get('gouv'))
                 ->setCity(/*$request->request->get('ville'*/"")
                 ->setZip((int)$request->request->get('zip'))
                 ->setUsername($username)
                 ->setPassword(
                    $passwordEncoder->encodePassword(
                         $user,
                         $pass
                     )
                 )
                 ->setEmail($request->request->get('mail'))
                 ->setEtat("Enabled")
                 ->setEnabled("yes");
                 $entityManager = $this->getDoctrine()->getManager();
                 $entityManager->persist($user);
                 $entityManager->flush();
                 $username=$this->generate_unique_username($request->request->get('prenom'),$request->request->get('nom'),$user->getId());
                 $user->setUsername($username);
                 $entityManager->persist($user);
                 $entityManager->flush();
                 
                 $message = (new \Swift_Message('Hello Email'))
                 ->setFrom('marvelamir@gmail.com')
                 ->setTo($user->getEmail())
                 ->setBody(
                     $this->renderView(
                         // templates/hello/email.txt.twig
                         'email/mail.txt.twig',
                         ['name' => "amir",
                            'username' => $username,
                            'pass' => $pass
                         ]
                     )
                 )
             ;
            
             $mailer->send($message);

            // do anything else you need here, like send an email

            return $this->redirectToRoute('app_register');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    

}
