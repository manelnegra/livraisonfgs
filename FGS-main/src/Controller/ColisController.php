<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Colis;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ColisController extends AbstractController
{
    /**
     * @Route("/formulaire", name="formulaire")
     */
    public function formulaire(Request $request){ 
        
        $user=$this->getUser();
        
        return $this->render('colis/formulaire.html.twig',[
            'user' => $user
        ]);
    }

    /**
     * @Route("/créer_colis", name="créer_colis")
     */
    public function creer_colis(Request $request , \Knp\Snappy\Pdf $snappy){
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $des = $request->request->get('gouv_c');
        $adr = $request->request->get('adr_c');
        $dest = $des."-".$request->request->get('ville_c')."-".$adr."-".$request->request->get('zip_c');
        if($request->request->count() > 0){
            $colis = new Colis();
            $colis->setTServ($request->request->get('tserv_c'))
                  ->setTColis($request->request->get('tcolis_c'))
                  ->setTEnlev($request->request->get('tenlev_c'))
                  ->setLongeur($request->request->get('long_c'))
                  ->setLargeur($request->request->get('larg_c'))
                  ->setHauteur($request->request->get('haut_c'))
                  ->setPoid($request->request->get('poid_c'))
                  ->setCreatedAt(new \DateTime('now'))
                  ->setEnlevAt(new \DateTime('now'))
                  ->setHubAt(new \DateTime('now'))
                  ->setDelivAt(new \DateTime('now'))
                  ->setDestination($dest)
                  ->setEtat("en attente e")
                  ->setCodeC($colis->getId()+1)
                  ->setRq($request->request->get('rq_c'))
                  ->setMPaie($request->request->get('tpaim_c'))
                  ->setNomC($request->request->get('nom_c'))
                  ->setPrenomC($request->request->get('pre_c'))
                  ->setTelC($request->request->get('tel_c'))
                  ->setEtats(['en attente e'])
                  ->setPrix($request->request->get('tprix_c'))
                  ->setFournisseur($user);

                  
                  $em->persist($colis);

        }
        $em->flush();

        $nid = strlen((string)$colis->getId());
        if ($nid < 5)
            $nid=5-$nid;
        $nidd = "";
        for($i=0;$i<$nid;$i++)
            $nidd=$nidd."0";
        if ($colis->getTServ()=="COD"){
            $tserv="99";

        }else if ($colis->getTServ()=="ECHG"){
            $tserv="88";
        }else
            $tserv="77";
        $codec = "A".$tserv."02"."01". $nidd.$colis->getId()."A";
        
        $barcode = new BarcodeGenerator();
        $barcode->setText($codec);
        $barcode->setType(BarcodeGenerator::Codabar);
        $barcode->setScale(2);
        $barcode->setThickness(25);
        $barcode->setFontSize(10);
        $barcode->setFilename("codeabars/colis/".$colis->getId().".png");
        $barcode->setFormat('PNG');
        $code = $barcode->generate();
        
$html = $this->render("colis/model.html.twig", [
            'title' => "Welcome to our PDF Test",
            'colis' => $colis,
            'code'=>$codec,
            'filename' => 'codeabars/colis/' . $colis->getId() . '   .png',
            'x' => 1
        ]);
        $filename = 'PDF';
        return new Response(
            $snappy->getOutputFromHtml($html),200,array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"',
                'image' => true
            )
        );


    }

    /**
     * @Route("/liste_colis_a" , name="liste_colis_a")
     * @IsGranted("ROLE_ADMIN")
     */
    public function liste_a(){
        $user = $this->getUser();
        $colis = $this->getDoctrine()
                ->getRepository(Colis::class)
                ->findAll();
                dump($colis);

                $users = $this->getDoctrine()
                ->getRepository(User::class)
                ->findAll();

                $i = 0;
                $x = array();
        
                foreach ($users as $user) {
                    if (!in_array('ROLE_LIV', $user->getRoles()) && !in_array('ROLE_ADMIN', $user->getRoles())) {
                        $x[$i] = $user;
                        $i++;
                    }
                }
        
        return $this->render("colis/liste.html.twig",[
            'colis' => $colis,
            'fourns' => $x,
            'x' => 1
        ]);
        


    }

    /**
     * @Route("/colis_print/{id}" , name="colis_print")
     * @IsGranted("ROLE_ADMIN")
     */
    public function print(Colis $colis , \Knp\Snappy\Pdf $snappy){
        $nid = strlen((string)$colis->getId());
        if ($nid < 5)
            $nid=5-$nid;
        $nidd = "";
        for($i=0;$i<$nid;$i++)
            $nidd=$nidd."0";
        if ($colis->getTServ()=="COD"){
            $tserv="99";

        }else if ($colis->getTServ()=="ECHG"){
            $tserv="88";
        }else
            $tserv="77";
        $codec = "A".$tserv."02"."01". $nidd.$colis->getId()."A";
        $html = $this->render("colis/model.html.twig", [
            'title' => "Welcome to our PDF Test",
            'colis' => $colis,
            'code'=>$codec,
            'filename' => 'codeabars/colis/' . $colis->getId() . '   .png',
            'x' => 1
        ]);
        $filename = 'PDF';
        return new Response(
            $snappy->getOutputFromHtml($html),200,array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"',
                'image' => true
            )
        );

    }
    /**
     * @Route("/liste_colis_f" , name="liste_colis_f")
     */
    public function listef(Request $request ){
        $user = $this->getUser();

                $users = $this->getDoctrine()
                ->getRepository(User::class)
                ->findAll();

                $i = 0;
                $x = array();
        
                foreach ($users as $user) {
                    if (!in_array('ROLE_LIV', $user->getRoles()) && !in_array('ROLE_ADMIN', $user->getRoles())) {
                        $x[$i] = $user;
                        $i++;
                    }
                }
                $colis = new Colis;
                $etat=$request->request->get('etat');
                $u=$request->request->get('user');
                if($u != "Fournisseur"){

                    if($etat == "Livre" ){
                            for ($i=0; $i <count($x) ; $i++) { 
                                    if ($x[$i]->getName() == $u )
                                        $h = $x[$i];
                            }
                            
                        $colis = $this->getDoctrine()
                        ->getRepository(Colis::class)
                        ->findBy(['etat' => "Livre", 'fournisseur' =>$h]);
                    }
                    if($etat == "Eneleve" ){
                        for ($i=0; $i <count($x) ; $i++) { 
                                if ($x[$i]->getName() == $u )
                                    $h = $x[$i];
                                    
                        }
                    $colis = $this->getDoctrine()
                    ->getRepository(Colis::class)
                    ->findBy(['etat' => "dispatched", 'fournisseur' => $h]);
                }
                if($etat == "Anomalie" ){
                    for ($i=0; $i <count($x) ; $i++) { 
                            if ($x[$i]->getName() == $u )
                                $h = $x[$i];
                    }
                    
                $colis = $this->getDoctrine()
                ->getRepository(Colis::class)
                ->findBy(['etat' => "anomalie", 'fournisseur' =>$h]);
            }
            if($etat == "Etat"){
                for ($i=0; $i <count($x) ; $i++) { 
                    if ($x[$i]->getName() == $u )
                        $h = $x[$i];
            }
            $colis = $this->getDoctrine()
                ->getRepository(Colis::class)
                ->findBy(['fournisseur' =>$h]);
        }
                }else if($etat == "Livre")
            $colis = $this->getDoctrine()
            ->getRepository(Colis::class)
            ->findBy(['etat' => "Livre"]);
            else if ($etat == "Eneleve")
            $colis = $this->getDoctrine()
            ->getRepository(Colis::class)
            ->findBy(['etat' => "dispatched"]);
            else
            $colis = $this->getDoctrine()
            ->getRepository(Colis::class)
            ->findBy(['etat' => "anomalie"]);
                
        
        return $this->render("colis/liste.html.twig",[
            'colis' => $colis,
            'fourns' => $x,
            'x' => 1
        ]);

    }

    /**
     * @Route("/fiche_colis/{id}", name="fiche_colis")
     */
    public function fiche($id){
        $colis = $this->getDoctrine()
        ->getRepository(Colis::class)
        ->find($id);
        dump($colis);
        return $this->render("colis/fiche.html.twig",[
            'colis' => $colis
        ]);
    }

}
