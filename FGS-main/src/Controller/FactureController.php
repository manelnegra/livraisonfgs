<?php

namespace App\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;
use App\Entity\User;
use App\Entity\Colis;
use App\Entity\Facture;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class FactureController extends AbstractController
{
    /**
     * @Route("/facture/", name="facture")
     */
    public function index(Request $request) 
    {
        $em = $this->getDoctrine()->getManager();
        $fourn=$request->request->get('tliv');
        $fourns = $this->getDoctrine()->getRepository(User::class)->findOneBy(['name' => $fourn]);
        $colis=$this->getDoctrine()->getRepository(Colis::class)->findBy(['fournisseur' => $fourns->getId(), 'etat' => "Livre" ]);

        $s = 0;
        $i=0;
        
        $fact = new Facture();
        $fact->setCreatedat(new \DateTime());
        $fact->setFournisseur($fourns);
        foreach ($colis as $coli){
            $s = $s + $coli->getPrix();
            $i++;
            $coli->setFacture($fact);
            ;
        }

        $s = $s ;
        $x = $i* $fourns->getPrixLiv() ;
        $x=$x+$x*0.07;
        $s=$s-$x;
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();


        // Instantiate Dompdf with our options
        $dompdf = new Dompdf();

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('facture/index.html.twig', [
            'colis' => $colis,
            'fourn' => $fourns,
            'montantc' => $s,
            'montantm' => $x 
     
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // Render the HTML as PDF
        $dompdf->render();
        $em->persist($fact);
        $em->flush();
        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);
    }
}
