<?php

namespace App\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;
use App\Entity\Colis;
use App\Entity\Retour;
use App\Entity\User;
use App\Repository\ColisRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RetourController extends AbstractController
{
    /**
     * @Route("/retour", name="retour")
     */
    public function index(ColisRepository $colisRepository): Response
    {
        $r=$this->getDoctrine()
                     ->getRepository(Retour::class)
                     ->findAll();
        $e=[];

        foreach ($r as $code => $ret) {
            $e[] = [
                'id' => $colisRepository->find($ret->getIdcolis()),
                'pth' => "/codeabars/colis/".$colisRepository->find($ret->getIdcolis())->getId()
            ];
        }

        dump($e);
        
        return $this->render('retour/index.html.twig', [
            'colis' => $e,
            'x' => 1
        ]);
    }

    /**
     * @Route("/create_liste_r" , name="create_liste_r")
     */

     public function create(){
         $retours = $this->getDoctrine()->getRepository(Retour::class)->findAll();
         $colis1 = [];
         foreach ($retours as $retour){
            $colis = $this->getDoctrine()->getRepository(Colis::class)->findOneBy(['id' => $retour->getIdcolis()]);
            $colis1[] = [
                'id' => $colis
            ];
         }

         // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);
         $liv = new User;
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('runsheet/model.html.twig', [
            'title' => "Welcome to our PDF Test",
            'Livreur' => $liv,
            'colis' => $colis1,
            'x' => 1
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);
     }
}
