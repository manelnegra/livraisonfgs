<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class , [
                'label' => false,
                
                    'attr' => [
                        'placeholder' => 'Nom'
                    ]
                
            ])
            ->add('lastname', TextType::class , [
                'label' => false,
                
                    'attr' => [
                        'placeholder' => 'Prénom'
                    ]
                
            ])
            ->add('tel', TextType::class , [
                'label' => false,
                
                    'attr' => [
                        'placeholder' => 'Téléphone'
                    ]
                
            ])
            ->add('cin', TextType::class , [
                'label' => false,
                
                    'attr' => [
                        'placeholder' => 'CIN'
                    ]
                
            ])
            ->add('matricule', TextType::class , [
                'label' => false,
                
                    'attr' => [
                        'placeholder' => 'Matricule du véhicule'
                    ]
                
            ])
            ->add('email', TextType::class , [
                'label' => false,
                
                    'attr' => [
                        'placeholder' => 'Email'
                    ]
                
            ])
            ->add('permis', FileType::class, [
                'label' => 'Photo du permis',
                'attr' => [
                    'accept' => 'image/jpeg, image/jpg, image/png, image/gif'
                ],
                'data_class' => null,
                'required' => false
            ]
        )
         
        ->add('photo', FileType::class, [
            'label' => 'Votre photo',
            'attr' => [
                'accept' => 'image/jpeg, image/jpg, image/png, image/gif'
            ],
            'data_class' => null,
            'required' => false
        ]
    )

    ->add('carteg', FileType::class, [
        'label' => 'Photo de la carte grise',
        'attr' => [
            'accept' => 'image/jpeg, image/jpg, image/png, image/gif'
        ],
        'data_class' => null,
        'required' => false
    ]
)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
