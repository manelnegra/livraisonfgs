<?php

namespace App\Repository;

use App\Entity\ListeRetour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ListeRetour|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListeRetour|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListeRetour[]    findAll()
 * @method ListeRetour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListeRetourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListeRetour::class);
    }

    // /**
    //  * @return ListeRetour[] Returns an array of ListeRetour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ListeRetour
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
